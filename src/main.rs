// Copyright (c) 2015, Dennis Hamester dennis.hamester@startmail.com
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
// ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
// WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
// ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
// OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

extern crate getopts;
extern crate toml;

use dotrix::{Dotrix, DotrixError, DotrixResult};
use getopts::Options;
use std::env;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};

mod dotrix;

const DEFAULT_SRC_DIR: &'static str = "dotfiles";

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

fn read_dottoml<S: AsRef<Path>>(src: S, dt: &mut Dotrix) -> DotrixResult<()> {
    let src = src.as_ref();
    let dottoml = src.join("dot.toml");
    let prefix = src.file_name().unwrap();

    match fs::metadata(&dottoml) {
        Ok(_) => { }

        Err(e) => {
            match e.kind() {
                io::ErrorKind::NotFound => { return Ok(()) }
                _ => { }
            }
        }
    }

    // Parse dot.toml first. If there are any syntatic errors, abort early.
    try!(dt.add_dottoml(&dottoml, prefix));

    // Check existing files
    for entry in fs::read_dir(src).unwrap() {
        let entry = entry.unwrap();
        let filename = entry.file_name();

        if &filename == "dot.toml" {
            continue;
        }

        dt.check_file(filename, prefix);
    }

    Ok(())
}

fn read_dotfiles<S: AsRef<Path>>(src: S, dt: &mut Dotrix) -> DotrixResult<()> {
    for entry in fs::read_dir(src).unwrap() {
        let entry = entry.unwrap();
        let file_type = entry.file_type().unwrap();

        if !file_type.is_dir() {
            continue;
        }

        let path = entry.path();
        try!(read_dottoml(&path, dt));
    }

    Ok(())
}

fn real_main() -> bool {
    let home = match env::home_dir() {
        Some(h) => { h }
        None => {
            println!("Failed to determine home directory.");
            return false;
        }
    };

    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optflag("h", "help", "print this help");
    opts.optopt("", "src", "source directory; defaults to '~/dotfiles/'", "DIR");
    opts.optopt("", "dst", "destination directory; defaults to '~/'", "DIR");
    opts.optflag("d", "dryrun", "don't create any symlinks");
    opts.optflag("", "version", "show dotrix version");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(e) => {
            println!("{}", e.to_string());
            println!("Try '{} --help' for more information.", program);
            return false;
        }
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return true;
    }

    if matches.opt_present("version") {
        println!("dotrix v{}", env!("CARGO_PKG_VERSION"));
        return true;
    }

    let src = match matches.opt_str("src") {
        Some(p) => { PathBuf::from(p) }
        None => { home.join(DEFAULT_SRC_DIR) }
    };

    let dst = match matches.opt_str("dst") {
        Some(p) => { PathBuf::from(p) }
        None => { home.clone() }
    };

    let dryrun = matches.opt_present("d");

    let mut dt = Dotrix::new();
    match read_dotfiles(&src, &mut dt) {
        Ok(_) => { }

        Err(e) => {
            match e {
                DotrixError::ParserErrors(e) => {
                    let dsp = e.file.display();
                    for error in e.errors {
                        println!("Error ({}): {}.", dsp, error);
                    }
                }

                DotrixError::IoError(e) => {
                    println!("Error ({}): {}.", e.file.display(), e.error);
                }

                DotrixError::InvalidDotTomlError(e) => {
                    println!("Error ({}): {}.", e.file.display(), e.desc);
                }
            }

            return false;
        }
    }

    dt.convert_to_symlinks(src, dst);
    let result = dt.run(dryrun);

    if dryrun {
        println!("Dryrun was active. No actual symlinks were created.");
    }

    return result;
}

fn main() {
    let result = real_main();
    if !result {
        std::process::exit(1);
    }
}
